import src.linear as linear
import os



def first_assignmnent():
    a = linear.Matrix(array=[[1, 2, 3.01, 4]])
    b = linear.Matrix(array=[[1, 2, 3, 4]])

    print(a == b)

def second_assignment():
    print('Drugi zadatak:')
    A = linear.Matrix(array=[[3, 9, 6], [4, 12, 12], [1, -1, 1]])
    b = linear.Matrix(file_name=os.path.join('..', 'res', 'b2.txt'))

    print('Ne možemo napravi LU dekompoziciju!')
    decomp, p = A.plu_decompose()

    # L*c = P*b
    p = linear.permutation_matrix(p)
    b = b.transpose()
    Pb = p * b
    Pb = Pb.transpose()
    c = decomp.forward_substitution(Pb)

    print(c)

    # U*x=c

    x = decomp.backward_substitution(c)
    print('rješenja: %s' % (x))


def third_assignment():
    A = linear.Matrix(file_name=os.path.join('..', 'res', 'A3.txt'))
    b = linear.Matrix(array=[[12, 12, 1]])

    lu = A.lu_decompose()
    print('LU dekompozicija:')
    print(lu)

    lup, p = A.plu_decompose()
    print('LUP dekompozicija:')
    print(lup)

    """
    #L*c = b
    c = lu.forward_substitution(b)

    #U*x = c
    x = lu.backward_substitution(c)

    print('rješenje sustava: %s'%(x))
    """

    # L*c = P*b
    p = linear.permutation_matrix(p)
    b = b.transpose()
    Pb = p * b
    Pb = Pb.transpose()
    c = lup.forward_substitution(Pb)

    # U*x=c

    x = lup.backward_substitution(c)
    print('rješenja: %s' % (x))


def fourth_assignment():
    A = linear.Matrix(file_name=os.path.join('..', 'res', 'A4.txt'))
    b = linear.Matrix(file_name=os.path.join('..', 'res', 'b4.txt'))

    lu = A.lu_decompose()
    print('LU dekompozicija:')
    print(lu)

    lup, p = A.plu_decompose()
    print('LUP dekompozicija:')
    print(lup)

    # L*c = b
    c = lu.forward_substitution(b)

    # U*x = c
    x = lu.backward_substitution(c)

    print('rješenje sustava sa LU: %s' % (x))

    # L*c = P*b
    p = linear.permutation_matrix(p)
    b = b.transpose()
    Pb = p * b
    Pb = Pb.transpose()
    c = lup.forward_substitution(Pb)

    # U*x=c

    x = lup.backward_substitution(c)
    print('rješenje sustava sa LUP: %s' % (x))


def fifth_assignment():
    A = linear.Matrix(file_name=os.path.join('..', 'res', 'A5.txt'))
    b = linear.Matrix(file_name=os.path.join('..', 'res', 'b5.txt'))

    lup, p = A.plu_decompose()
    print('LUP dekompozicija:')
    print(lup)

    # L*c = P*b
    p = linear.permutation_matrix(p)
    b = b.transpose()
    Pb = p * b
    Pb = Pb.transpose()
    c = lup.forward_substitution(Pb)

    # U*x=c

    x = lup.backward_substitution(c)
    print('rješenje sustava sa LUP: %s' % (x))


def sixth_assignment():
    A = linear.Matrix(file_name=os.path.join('..', 'res', 'A6.txt'))
    b = linear.Matrix(file_name=os.path.join('..', 'res', 'b6.txt'))

    lup, p = A.plu_decompose()
    print('LUP dekompozicija:')
    print(lup)

    # L*c = P*b
    p = linear.permutation_matrix(p)
    b = b.transpose()
    Pb = p * b
    Pb = Pb.transpose()
    c = lup.forward_substitution(Pb)

    # U*x=c

    x = lup.backward_substitution(c)
    print('rješenje sustava sa LUP: %s' % (x))


if __name__ == '__main__':
    num = int(input('Unesite broj zadatka:'))

    if num == 1:
        first_assignmnent()
    elif num == 2:
        second_assignment()
    elif num == 3:
        third_assignment()
    elif num == 4:
        fourth_assignment()
    elif num == 5:
        fifth_assignment()
    elif num == 6:
        sixth_assignment()
    else:
        print("pogrešan unos!")
