"""

"""

import numbers
import copy


def identity_matrix(dim, base=1.0):
    """

    :param dim
    :param base:
    :return:
    """
    data = [dim * [0] for _ in range(dim)]
    for i in range(dim):
        data[i][i] = base

    return Matrix(array=data)


def permutation_matrix(perm_vec):
    """

    :param perm_vec:
    :return:
    """
    perm_vec = perm_vec[0]
    p_array = [[0] * len(perm_vec) for _ in range(len(perm_vec))]

    for i in range(len(perm_vec)):
        p_array[i][perm_vec[i]] = 1

    return Matrix(array=p_array)


class Matrix:
    """

    """

    def __init__(self, file_name=None, other_matrix=None, array=None):
        """

        :param file_name:
        :param other_matrix:
        """
        self._data = []
        self._rows = 0
        self._columns = 0

        if file_name:
            self._read_from_file(file_name)
        elif other_matrix:
            self._read_from_matrix(other_matrix)
        elif array:
            self._read_from_array(array)

    """
    INSTANCE METHODS
    """

    def set(self, row, column, value):
        """

        :param row:
        :param column:
        :param value:
        :return:
        """
        self._data[row][column] = value

    def resize(self, rows, columns):
        """

        :param rows:
        :param columns:
        :return:
        """
        new = list(self.data)

        if rows < self.rows:
            new = new[:rows]
        elif rows > self.rows:
            new = new + (rows - self.rows) * [self.columns * [0]]

        if columns < self.columns:
            new = [row[:columns] for row in new]
        elif columns > self.columns:
            new = [row + [0] * (columns - self.columns) for row in
                   new]

        return Matrix(array=new)

    def write(self, file_name=None):
        """

        :param file_name:
        :return:
        """
        if not file_name:
            print(str(self))

        else:
            with open(file_name, 'w') as fp:
                fp.write(str(self))

    def add(self, other):
        """

        :param other:
        :return:
        """
        if isinstance(other, numbers.Number):
            other = identity_matrix(self.rows, other)

        self._check_dimensions(other)

        def matrix_add(i, j, value):
            return self[i][j] + value[i][j]

        return self._apply_on_all_elements(matrix_add, other)

    def sub(self, other):
        """

        :return:
        """
        if isinstance(other, numbers.Number):
            other = identity_matrix(self.rows, other)

        self._check_dimensions(other)

        def matrix_sub(i, j, value):
            return self[i][j] - value[i][j]

        return self._apply_on_all_elements(matrix_sub, other)

    def mul(self, other):
        """

        :param other:
        :return:
        """
        if isinstance(other, Matrix):
            self._check_matrix_mul(other)

            res = [[0] * other.columns for i in range(self.rows)]

            for i in range(len(self)):
                for j in range(len(other[0])):
                    for k in range(len(other)):
                        res[i][j] += self[i][k] * other[k][j]

            return Matrix(array=res)

        elif isinstance(other, numbers.Number):
            def number_mul(i, j, value):
                return self[i][j] * value

            return self._apply_on_all_elements(number_mul, other)

        else:
            raise TypeError

    def forward_substitution(self, vector):
        """

        :param vector:
        :return:
        """
        if len(vector[0]) != len(self):
            raise ValueError("The matrix and the coefficient vector must be "
                             "of same dimension.")

        res = len(vector[0]) * [0]

        for i in range(len(res)):
            res[i] = (vector[0][i] - sum([self[i][j] * res[j] for j in range(
                i)])) if i != 0 else vector[0][i]

        return Matrix(array=[res])

    def backward_substitution(self, vector):
        """

        :param vector:
        :return:
        """
        if len(vector[0]) != len(self):
            raise ValueError("The matrix and the coefficient vector must be "
                             "of same dimension.")

        res = len(vector[0]) * [0]

        for i in range(len(res) - 1, -1, -1):
            res[i] = ((vector[0][i] - sum(
                [self[i][j] * res[j] for j in range(i + 1, len(res))]))
                      / self[i][i]) if (i != len(res) - 1) else (
                    vector[0][i] / self[i][i])

        return Matrix(array=[res])

    def lu_decompose(self):
        """

        :return:
        """
        if self.rows != self.columns:
            raise TypeError("The matrix must be square for decomposition")

        lu = self.clone()

        for i in range(self.rows - 1):
            for j in range(i + 1, self.rows):

                try:
                    lu.set(j, i, lu[j][i] / lu[i][i])
                except ZeroDivisionError:
                    raise ValueError("The pivot element must be a non zero "
                                     "value.")

                for k in range(i + 1, self.rows):
                    lu.set(j, k, lu[j][k] - (lu[j][i] * lu[i][k]))

        return lu

    def plu_decompose(self):
        """

        :return:
        """
        if self.rows != self.columns:
            raise TypeError("The matrix must be square for decomposition")

        p = [i for i in range(self.rows)]
        lu = self.clone()

        for i in range(self.rows - 1):
            pivot = i

            for j in range(i + 1, self.rows):
                if abs(lu[j][i]) > abs(lu[pivot][i]):
                    pivot = j

            p[i], p[pivot] = p[pivot], p[i]
            lu = lu.swap_rows(i, pivot)

            for j in range(i + 1, self.rows):
                lu.set(j, i, lu[j][i] / lu[i][i])

                for k in range(i + 1, self.rows):
                    lu.set(j, k, lu[j][k] - (lu[j][i]) * lu[
                        i][k])

        return lu, Matrix(array=[p])

    def transpose(self):
        """

        :return:
        """
        return Matrix(array=[[self[j][i] for j in range(len(self))] for i in
                             range(len(self[0]))])

    def swap_rows(self, first_index, second_index):
        """

        :param first_index:
        :param second_index:
        :return:
        """
        if first_index == second_index:
            return self

        res = list(self.data)

        res[first_index], res[second_index] = res[second_index], res[
            first_index]

        return Matrix(array=res)

    def clone(self):
        """

        :return:
        """
        return Matrix(array=list(self.data))

    """
    PROPERTY METHODS
    """

    @property
    def data(self):
        """

        :return:
        """
        return self._data

    @property
    def rows(self):
        """

        :return:
        """
        return self._rows

    @property
    def columns(self):
        """

        :return:
        """
        return self._columns

    """
    MAGIC METHODS AND OPERATOR OVERLOADING
    """

    def __setitem__(self, key, value):
        # TODO
        raise NotImplementedError

    def __getitem__(self, item):
        return self.data[item]

    def __add__(self, other):
        return self.add(other)

    def __sub__(self, other):
        return self.sub(other)

    def __eq__(self, other):
        return self.element_eq(other)

    def element_eq(self, other):
        for i in range(len(self.data)):
            for j in range(len(self.data[0])):
                if abs(self[i][j] - other.data[i][j]) > pow(10, -5):
                    return False

        return True

    def __radd__(self, other):
        return self.add(other)

    def __str__(self):
        txt = ""
        for row in self.data:
            row_txt = ''
            for elem in row:
                row_txt += str(elem) + ' '

            txt += row_txt[:-1] + '\n'

        return txt

    def __len__(self):
        return len(self.data)

    def __mul__(self, other):
        return self.mul(other)

    def __neg__(self):
        def negate(i, j, other):
            return -self[i][j]

        return self._apply_on_all_elements(negate)

    def __copy__(self):
        return Matrix(self)

    def __deepcopy__(self, memodict={}):
        return self.__copy__()

    """
    PRIVATE METHODS
    """

    def _check_matrix_mul(self, other):
        """

        :param other:
        :return:
        """
        if self.columns != other.rows:
            raise TypeError("incompatible matrix dimensions for "
                            "multiplication")

    def _check_dimensions(self, other):
        """

        :param other:
        :return:
        """
        if self.rows != other.rows:
            raise ValueError('incompatible number of rows')
        elif self.columns != other.columns:
            raise ValueError('incompatible number of columns')

    def _apply_on_all_elements(self, action, other=None):
        """

        :param action:
        :return:
        """
        res = []
        for i in range(self.rows):
            row = self.rows * [0]
            for j in range(self.columns):
                row[j] = action(i, j, other)

            res.append(row)

        return Matrix(array=res)

    def _read_from_file(self, file_name):
        """

        :param file_name:
        :return:
        """
        with open(file_name, 'r') as fp:
            for line in fp:
                self._data.append(
                    [float(elem.strip()) for elem in line.strip().split()])

        self._columns = len(self._data[0])
        self._rows = len(self._data)

    def _read_from_matrix(self, other):
        """

        :param other:
        :return:
        """
        self._data = copy.deepcopy(other.data)
        self._rows = other.rows
        self._columns = other.columns

    def _read_from_array(self, array):
        """

        :param array:
        :return:
        """
        self._data = array
        self._columns = len(array[0])
        self._rows = len(array)
