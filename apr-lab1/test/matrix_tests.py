import unittest as ut
import sys
import os

sys.path.append('..')

import src.linear as linear


class TestMatrixMethods(ut.TestCase):

    def test_from_file(self):
        A = linear.Matrix(file_name=os.path.join('..', 'res', 'A.txt'))
        tmp = [[12.5, 3.0, 9, 2], [4, 5, 6, 7], [8, 9, 10, 11]]

        self.assertEqual(A.data, tmp)

    def test_resize(self):
        A = linear.Matrix(file_name=os.path.join('..', 'res', 'A.txt'))
        B = A.resize(2, 2)

        self.assertEqual(B.data, [[12.5, 3.0], [4, 5]])

        C = B.resize(3, 2)
        self.assertEqual([[12.5, 3.0], [4, 5], [0, 0]], C.data)

        D = B.resize(2, 2)
        self.assertEqual(D.data, B.data)

    def test_write(self):
        A = linear.Matrix(file_name=os.path.join('..', 'res', 'A.txt'))
        A.write()

    def test_add_number(self):
        A = linear.Matrix(array=[[1, 2], [3, 4]])
        self.assertEqual(A.add(3).data, [[4, 2], [3, 7]])

    def test_add_matrix(self):
        A = linear.Matrix(array=[[1, 2], [3, 4]])
        B = linear.Matrix(array=[[2, 2], [2, 2]])
        self.assertEqual(A.add(B).data, [[3, 4], [5, 6]])

    def test_add_matrix_2(self):
        A = linear.Matrix(array=[[1, 2], [3, 4]])
        B = linear.Matrix(array=[[2, 2], [2, 2], [1, 1]])
        self.failUnlessRaises(ValueError, lambda: A.add(B))

    def test_sub_number(self):
        A = linear.Matrix(array=[[1, 2], [3, 4]])
        self.assertEqual(A.sub(3).data, [[-2, 2], [3, 1]])

    def test_sub_matrix(self):
        A = linear.Matrix(array=[[1, 2], [3, 4]])
        B = linear.Matrix(array=[[2, 2], [2, 2]])
        self.assertEqual(A.sub(B).data, [[-1, 0], [1, 2]])

    def test_sub_matrix_2(self):
        A = linear.Matrix(array=[[1, 2], [3, 4]])
        B = linear.Matrix(array=[[2, 2], [2, 2], [1, 1]])
        self.failUnlessRaises(ValueError, lambda: A.add(B))

    def test_number_mul(self):
        A = linear.Matrix(array=[[1, 2], [3, 4]])
        self.assertEqual(A.mul(2).data, [[2, 4], [6, 8]])

    def test_matrix_mul(self):
        A = linear.Matrix(array=[[1, 2], [3, 4]])
        B = linear.Matrix(array=[[2, 2], [2, 2]])
        self.assertEqual(A.mul(B).data, [[6, 6], [14, 14]])

    def test_matrix_mul_2(self):
        A = linear.Matrix(array=[[1, 2], [3, 4], [5, 6]])
        B = linear.Matrix(array=[[2, 2], [2, 2]])
        self.assertEqual(A.mul(B).data, [[6, 6], [14, 14], [22, 22]])

    def test_matrix_mul3(self):
        A = linear.Matrix(array=[[1, 2, 3], [4, 5, 6]])
        B = linear.Matrix(array=[[2, 2], [2, 2]])
        self.failUnlessRaises(TypeError, lambda: A.mul(B))

    def test_transpose(self):
        A = linear.Matrix(array=[[1, 2], [3, 4]])
        self.assertEqual(A.transpose().data, [[1, 3], [2, 4]])

    def test_transpose_2(self):
        A = linear.Matrix(array=[[1, 2, 3], [4, 5, 6]])
        self.assertEqual(A.transpose().data, [[1, 4], [2, 5], [3, 6]])

    def test_overload_mul(self):
        A = linear.Matrix(array=[[1, 2], [3, 4]])
        B = linear.Matrix(array=[[2, 2], [2, 2]])
        self.assertEqual((A * B).data, [[6, 6], [14, 14]])

    def test_overload_add(self):
        A = linear.Matrix(array=[[1, 2], [3, 4]])
        B = linear.Matrix(array=[[2, 2], [2, 2]])
        self.assertEqual((A + B).data, [[3, 4], [5, 6]])

    def test_overload_sub(self):
        A = linear.Matrix(array=[[1, 2], [3, 4]])
        B = linear.Matrix(array=[[2, 2], [2, 2]])
        self.assertEqual((A - B).data, [[-1, 0], [1, 2]])

    def test_forward_substitution(self):
        A = linear.Matrix(array=[[1, 0, 0], [1, 1, 0], [2, -1 / 3, 1]])
        b = linear.Matrix(array=[[4, -6, 7]])
        self.assertEqual(A.forward_substitution(b).data[0], [4, -10,
                                                             -13 / 3])

    def test_backward_substitution(self):
        A = linear.Matrix(array=[[1, 1, -1], [0, -3, 4], [0, 0, 13 / 3]])
        b = linear.Matrix(array=[[4, -10, -13 / 3]])
        self.assertEqual(A.backward_substitution(b).data[0],
                         [1, 2, -1])

    def test_lu_decomposition(self):
        A = linear.Matrix(array=[[1, 4, -3], [-2, 8, 5], [3, 4, 7]])
        self.assertEqual(A.lu_decompose().data, [[1, 4, -3], [-2, 16, -1], [3,
                                                                            -0.5,
                                                                            15.5]])

    def test_lup_decomposition(self):
        A = linear.Matrix(array=[[2, 1, 5], [4, 4, -4], [1, 3, 1]])
        B, p = A.plu_decompose()
        self.assertEqual(B.data, [[4, 4, -4], [0.25, 2,
                                               2], [0.5,
                                                    -0.5,
                                                    8]])
        self.assertEqual(p.data[0], [1, 2, 0])

    def test_lup_decomposition2(self):
        A = linear.Matrix(array=[[3, 9, 6], [4, 12, 12], [1, -1, 1]])
        LUP, p = A.plu_decompose()
        self.assertEqual(LUP.data, [[4, 12, 12], [0.25, -4,
                                                -2], [0.75,
                                                      0,
                                                      1]])
        self.assertEqual(p.data[0], [2, 0, 1])

        b = linear.Matrix(array=[[12, 12, 1]])

        p = linear.permutation_matrix(p)

        Pb = p * b
        Pb = Pb.transpose()
        c = LUP.forward_substitution(Pb)

        self.assertEqual(c.data[0], [12, -2, 3])

        # U*x=c

        x = LUP.backward_substitution(c)
        print('rješenja: %s' % (x))
