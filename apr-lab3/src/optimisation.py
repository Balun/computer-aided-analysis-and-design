"""
"""

import numpy as np
import math

from functools import lru_cache
from scipy.optimize import minimize

GOLDEN_RATIO = (math.sqrt(5) - 1) / 2


def gradient_descent(f, x_start, golden_section=False,
                     eps=1e-6, max_iter=1000, trace=False):
    """

    :param f:
    :param x_start:
    :param eps:
    :return:
    """
    x = np.array(x_start)
    grad = np.array(f.gradient(x))

    i = 0
    while (abs(np.linalg.norm(grad)) > eps) and (i < max_iter):
        if trace:
            print('iter: %d, gradient: %s' % (i, grad))
        i += 1

        v = -grad

        if golden_section:
            def opt(l):
                return f(x + l * v)

            l, r = unimodal_interval(opt, 1, 0)
            alpha = golden_section_search(l, r, opt)

            np.add(x, alpha * v, out=x, casting="unsafe")

        else:
            np.add(x, v, out=x, casting="unsafe")

        grad = np.array(f.gradient(x))

    return x


def newton_raphson(f, x_start, golden_section=False, eps=1e-6,
                   max_iter=10000, trace=False):
    """

    :param f:
    :param x_start:
    :param golden_section:
    :param eps:
    :return:
    """
    x = np.array(x_start)
    grad = np.ones_like(x_start)
    i = 0

    while (np.linalg.norm(grad) > eps) and (i < max_iter):
        if trace:
            print('iter: ', i)
        i += 1

        hess = np.array(f.hessian(x))
        grad = np.array(f.gradient(x))

        grad = np.dot(np.linalg.inv(hess),
                      grad.reshape(-1, 1)).ravel()

        if golden_section:
            def opt(l):
                return f(x - l * grad)

            l, r = unimodal_interval(opt, 0.1, 1)
            l = golden_section_search(l, r, opt)

            x -= l * grad

        else:
            x -= grad

    return x


def box(f, x_start, lower, upper, implicit=None, alpha=1.3,
        eps=1e-6, trace=False):
    """

    :param f:
    :param x_start:
    :param alpha:
    :param eps:
    :return:
    """
    x_start = np.array(x_start)
    x_c = x_start

    points = np.random.uniform(lower, upper, [2 * len(x_start), len(x_start)])

    for i in range(len(points)):
        while np.any(implicit(*points[i]) < 0):
            points[i] = 0.5 * (points[i] + x_c)

    iter = 0
    while np.mean(np.linalg.norm(points - x_c, axis=0)) > eps:
        if trace:
            print('iter:', iter)
        iter += 1

        y = [f(x) for x in points]
        indexes = np.argsort(y)

        worst = indexes[-1]
        second_worst = indexes[-2]

        x_c = np.mean(points[indexes[:-1]], axis=0)

        x_r = (1 + alpha) * x_c - alpha * points[worst]
        x_r = np.clip(x_r, lower, upper)

        while np.any(implicit(*x_r) < 0):
            x_r = 0.5 * (x_r + x_c)

        while f(x_r) > y[second_worst]:
            x_r = 0.5 * (x_r + x_c)

        points[worst] = x_r

    return x_c


def limit_problem_transformation(f, x_0, implicit, equ=lambda x: 0, t=1,
                                 eps=1e-6):
    x_0 = np.array(x_0)

    if np.any(implicit(*x_0) < 0) or True:
        def u(x):
            return np.sum(np.square(np.maximum(0, -implicit(*x)))) + np.sum(
                np.square(equ(*x)))

        res = hooke_jeeves_search(u, x_0)

        if u(res) > eps:
            raise "Cannot find interior point" + str(res)

        x_0 = res

    x = x_0

    while True:
        def u(x):
            return f(x) + np.sum(np.where(implicit(*x) > 0, -1 / t * np.log(
                implicit(*x)), np.inf)) + t * np.sum(np.square(equ(*x)))

        res = np.array(hooke_jeeves_search(u, x))
        if np.linalg.norm(res - x) < eps:
            return res

        else:
            x = res
            t *= 10


def golden_section_search(a, b, f, eps=1e-6, trace=False):
    """

    :param a:
    :param b:
    :param f:
    :param eps:
    :return:
    """
    c = b - GOLDEN_RATIO * (b - a)
    d = a + GOLDEN_RATIO * (b - a)
    i = 0

    while (b - a) > eps:
        if trace:
            print('step %d: f(a=%s)=%f, f(a=%s)=%f, f(a=%s)=%f, f(a=%s)=%f' %
                  (i, str(a), f(a), str(b), f(b), str(c), f(c), str(d), f(d)))

        if f(c) < f(d):
            b = d
            d = c
            c = b - GOLDEN_RATIO * (b - a)

        else:
            a = c
            c = d
            d = a + GOLDEN_RATIO * (b - a)

        i += 1

    return (a + b) / 2


def unimodal_interval(f, h, x):
    """

    :param h:
    :param x_start:
    :param f:
    :return:
    """
    f = lru_cache(10)(f)
    l, r = x - h, x + h
    step = 1

    if f(l) > f(x) < f(r):
        return l, r

    if f(l) < f(x) < f(r):
        while f(l) < f(x):
            l -= step * h
            step *= 2
        return l, r
    else:
        while f(r) < f(x):
            r += step * h
            step *= 2
        return l, r


def hooke_jeeves_search(f, x_0, d_x=0.5, e=1e-6, trace=False):
    """

    :param f:
    :param x_0:
    :param d_x:
    :param e:
    :return:
    """
    x_b = list(x_0)
    x_p = list(x_0)
    i = 0

    while True:
        x_n = _explore(f, x_p, d_x)

        if f(x_n) < f(x_b):
            x_p = [(2 * n - b) for n, b in zip(x_n, x_b)]
            x_b = list(x_n)

        else:
            d_x /= 2
            x_p = list(x_b)

        if trace:
            print("step %d: f(Xb=%s)=%f, f(Xp=%s)=%f, f(Xn=%s)=%f" %
                  (i, str(x_b), f(x_b), str(x_p), f(x_p), str(x_n), f(x_n)))

        i += 1

        if d_x < e:
            return x_b


def _explore(f, x_p, d_x):
    """

    :param f:
    :param x_p:
    :param d_x:
    :return:
    """
    x = list(x_p)

    for i in range(len(x)):
        x_upper = x[:i] + [x[i] + d_x] + x[i + 1:]
        x_lower = x[:i] + [x[i] - d_x] + x[i + 1:]

        if f(x_upper) < f(x):
            x = x_upper

        elif f(x_lower) < f(x):
            x = x_lower

    return x
