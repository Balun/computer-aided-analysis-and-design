"""
"""


class Function:
    """
    """

    def __init__(self, name, f, x_0, x_min, f_min, grad=None, hessian=None):
        """

        :param name:
        :param f:
        :param x_0:
        :param x_min:
        :param f_min:
        """
        self.name = name
        self._f = f
        self._x_0 = x_0
        self._x_min = x_min
        self._f_min = f_min
        self._count = 0
        self._count_grad = 0
        self._count_hess = 0
        self._grad = grad
        self._hessian = hessian

    def eval(self, *args):
        """

        :param params:
        :return:
        """
        self._count += 1
        return self._f(*args)

    def reset(self):
        """

        :return:
        """
        self._count = 0
        self._count_hess = 0
        self._count_grad = 0

    def gradient(self, x):
        """

        :return:
        """
        if not self._grad:
            raise NotImplementedError()

        self._count_grad += 1
        return self._grad(*x)

    def hessian(self, x):
        if not self._hessian:
            raise NotImplementedError()

        self._count_hess += 1
        return self._hessian(*x)

    @property
    def start(self):
        """

        :return:
        """
        return self._x_0

    @property
    def minimum(self):
        """

        :return:
        """
        return self._f_min

    @property
    def x_min(self):
        """

        :return:
        """
        return self._x_min

    @property
    def function(self):
        """

        :return:
        """
        return self._f

    @property
    def count(self):
        """

        :return:
        """
        return self._count

    @property
    def hess_count(self):
        return self._count_hess

    @property
    def grad_count(self):
        return self._count_grad

    def __call__(self, args):
        """

        :param args:
        :param kwargs:
        :return:
        """
        return self.eval(*args)


FUNCTIONS = {
    'f1': Function('f1',
                   lambda x, y: 100 * ((y - x * x) ** 2) + ((1 - x) ** 2),
                   [-1.9, 2], [1, 1], 0,
                   lambda x, y: [200 * (200 * x ** 3 - 200 * x * y + x - 1),
                                 200 * (y - x ** 2)],
                   lambda x, y: [[2 * (600 * x ** 2 - 200 * y + 1), -400 * x],
                                 [
                                     -400 * x, 200]]),

    'f2': Function('f2',
                   lambda x, y: (x - 4) ** 2 + 4 * (y - 2) ** 2,
                   [0.1, 0.3], [4, 2], 0,
                   lambda x, y: [2 * (x - 4), 8 * (y - 2)],
                   lambda x, y: [[2, 0], [0, 8]]),

    'f3': Function('f3', lambda x, y: (x - 2) ** 2 + (y + 3) ** 2, [0, 0], [2,
                                                                            -3],
                   0, lambda x, y: [2 * (x - 2), 2 * (y + 3)]),

    'f4': Function('f4', lambda x, y: (x - 3) ** 2 + y ** 2, [0, 0], [3, 0],
                   0, lambda x, y: [2 * (x - 3), 2 * y])
}


def get(name):
    """

    :param name:
    :return:
    """
    return FUNCTIONS[name]
