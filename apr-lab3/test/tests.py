import src.optimisation as opt
import src.functions as func
import numpy as np


def first():
    f = func.get('f3')

    x1 = opt.gradient_descent(f, f.start, golden_section=True,
                              max_iter=10000)

    x2 = opt.gradient_descent(f, f.start, golden_section=False,
                              max_iter=10000)

    print()
    print("With line search x_min: ", x1)
    print("Without line search x_min: ", x2)


def second():
    f1 = func.get('f1')
    f2 = func.get('f2')

    x_min = opt.gradient_descent(f1, f1.start, True, max_iter=10000)
    print("x1_min, gradient descent: ", x_min)
    print('function count: %d; grad count: %d; hessian count: %d'
          % (f1.count, f1.grad_count, f1.hess_count))
    f1.reset()

    x_min = opt.gradient_descent(f2, f2.start, True, max_iter=10000)
    print("\nx2_min, gradient descent: ", x_min)
    print('function count: %d; grad count: %d; hessian count: %d'
          % (f2.count, f2.grad_count, f2.hess_count))
    f2.reset()

    print('\nx1_min, newton-raphson: ', opt.newton_raphson(f1, f1.start, True))
    print('function count: %d; grad count: %d; hessian count: %d'
          % (f1.count, f1.grad_count, f1.hess_count))
    f1.reset()

    print('\nx2_min, newton-raphson: ', opt.newton_raphson(f2, f2.start, True))
    print('function count: %d; grad count: %d; hessian count: %d'
          % (f2.count, f2.grad_count, f2.hess_count))
    f2.reset()


def third():
    implicit = lambda x, y: np.array([y - x, 2 - x])

    f1 = func.get('f1')
    f2 = func.get('f2')

    print("x1_min = ", opt.box(f1, f1.start, -100, 100, implicit))
    print("x2_min = ", opt.box(f2, f2.start, -100, 100, implicit))


def fourth():
    implicit = lambda x, y: np.array([y - x, 2 - x])

    f1 = func.get('f1')
    f2 = func.get('f2')

    print("x1_min = ", opt.limit_problem_transformation(f1, f1.start,
                                                        implicit))
    print("x2_min = ",
          opt.limit_problem_transformation(f2, f2.start, implicit))


def fifth():
    implicit = lambda x, y: np.array([3 - x - y, 3 + 1.5 * x - y, y - 1])
    explicit = lambda x, y: x - 1

    f = func.get('f4')

    print("x4_min = ", opt.limit_problem_transformation(f, [5, 5],
                                                        implicit, explicit))


if __name__ == '__main__':
    choice = int(input("Enter the number of the assignment: "))

    if choice == 1:
        first()
    elif choice == 2:
        second()
    elif choice == 3:
        third()
    elif choice == 4:
        fourth()
    elif choice == 5:
        fifth()
    else:
        print("Invalid option.")
