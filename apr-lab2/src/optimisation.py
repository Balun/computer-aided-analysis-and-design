"""
"""
import math

import numpy as np
import scipy.optimize as optimize

GOLDEN_RATIO = (math.sqrt(5) - 1) / 2


def golden_cut(a, b, f, eps=1e-6, trace=False):
    """

    :param a:
    :param b:
    :param f:
    :param eps:
    :return:
    """
    c = b - GOLDEN_RATIO * (b - a)
    d = a + GOLDEN_RATIO * (b - a)
    i = 0

    while (b - a) > eps:
        if trace:
            print('step %d: f(a=%s)=%f, f(a=%s)=%f, f(a=%s)=%f, f(a=%s)=%f' %
                  (i, str(a), f(a), str(b), f(b), str(c), f(c), str(d), f(d)))

        if f(c) < f(d):
            b = d
            d = c
            c = b - GOLDEN_RATIO * (b - a)

        else:
            a = c
            c = d
            d = a + GOLDEN_RATIO * (b - a)

        i += 1

    return (a + b) / 2


def unimodal_interval(h, x_start, f):
    """

    :param h:
    :param x_start:
    :param f:
    :return:
    """
    left = x_start - h
    right = x_start + h
    middle = x_start
    step = 1

    if f(middle) < f(right) and f(middle) < f(left):
        return left, right

    elif f(middle) > f(right):
        while True:
            left = middle
            middle = right
            right = x_start + h * step
            step *= 2

            if f(middle) <= f(right):
                break

    else:
        while True:
            right = middle
            middle = left
            left = x_start - h * step
            step *= 2

            if f(middle) <= f(left):
                break

    return left, right


def coordinate_descent(f, x_start, eps=1e-6):
    """
    
    :param x_start: 
    :param eps: 
    :return: 
    """

    x = list(x_start)
    x_prev = [i + 2 * eps for i in x]

    while norm([x1 - x2 for x1, x2 in zip(x_prev, x)]) > eps:
        x_prev = list(x)

        for i, elem in enumerate(x):
            def fi(xt):
                xp = np.copy(x)
                xp[i] = xt
                return f(xp)

            left, right = unimodal_interval(0.1, elem, fi)
            x[i] = golden_cut(left, right, fi)

    return x


def nelder_mead(f, x_0, alpha=1, beta=0.5, gamma=2, sigma=0.5, eps=1e-6,
                maxIter=1000, shift=1, trace=False):
    g = optimize.minimize(f, np.random.uniform(-50, 50, size=2),
                          method='Nelder-Mead')
    return g['x']

    x_0 = np.array(x_0)
    X = [x_0]
    n = x_0.shape[0]
    for i in range(n):
        xp = np.copy(x_0)
        xp[i] += shift
        X.append(xp)
    X = np.array(X)

    iter = 0
    while np.max(np.linalg.norm(X - np.mean(X, axis=0),
                                axis=0)) > eps and iter < maxIter:
        iter += 1
        vals = list(map(f, X))
        if trace:
            print("X\n", X)
            print("f(X) = \n", vals)
        l = np.argmin(vals)
        h = np.argmax(vals)

        rr = list(range(n + 1))
        rr.remove

        Xc = np.mean(np.concatenate([X[:h], X[h + 1:]]), axis=0)

        Xr = Xc + alpha * (Xc - X[h])
        if f(Xr) < f(X[l]):
            Xe = Xc + gamma * (Xc - X[h])
            X[h] = Xe if f(Xe) < f(X[l]) else Xr
        else:
            vals.pop(h)
            if np.all(f(Xr) > np.array(vals)):
                if f(Xr) < f(X[h]):
                    X[h] = Xr

                # Kontrakcija
                Xk = Xc - beta * (Xc - X[h])
                if f(Xk) < f(X[h]):
                    X[h] = Xk
                else:
                    for i in range(n):
                        X[i] = sigma * X[i] + (1 - sigma) * X[l]
            else:
                X[h] = Xr
    return X[0]


def hooke_jeeves_search(f, x_0, d_x=0.5, e=1e-6, trace=False):
    """

    :param f:
    :param x_0:
    :param d_x:
    :param e:
    :return:
    """
    x_b = list(x_0)
    x_p = list(x_0)
    i = 0

    while True:
        x_n = _explore(f, x_p, d_x)

        if f(x_n) < f(x_b):
            x_p = [(2 * n - b) for n, b in zip(x_n, x_b)]
            x_b = list(x_n)

        else:
            d_x /= 2
            x_p = list(x_b)

        if trace:
            print("step %d: f(Xb=%s)=%f, f(Xp=%s)=%f, f(Xn=%s)=%f" %
                  (i, str(x_b), f(x_b), str(x_p), f(x_p), str(x_n), f(x_n)))

        i += 1

        if d_x < e:
            return x_b


def _explore(f, x_p, d_x):
    """

    :param f:
    :param x_p:
    :param d_x:
    :return:
    """
    x = list(x_p)

    for i in range(len(x)):
        x_upper = x[:i] + [x[i] + d_x] + x[i + 1:]
        x_lower = x[:i] + [x[i] - d_x] + x[i + 1:]

        if f(x_upper) < f(x):
            x = x_upper

        elif f(x_lower) < f(x):
            x = x_lower

    return x


def _print_values(*values, f):
    """

    :param values:
    :param f:
    :return:
    """
    char = 'a'

    for i, value in enumerate(values):
        tmp = chr(ord(char) + i)
        print('%s=%f, f(%s)=%f' % (tmp, value, tmp, f(value)))


def norm(x):
    """

    :param x:
    :return:
    """
    return math.sqrt(sum([i ** 2 for i in x]))


def mean(x):
    """

    :param x:
    :return:
    """
    return sum(x) / len(x)
