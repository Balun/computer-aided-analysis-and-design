"""
"""
import src.optimisation as opt
import src.functions as func

import random
import sys
import pandas as pd


def first_task():
    f = func.get("f3")

    for i in [10, 20, 30]:
        for alg in [opt.coordinate_descent, opt.hooke_jeeves_search,
                    opt.nelder_mead]:
            print(alg.__name__, alg(f, [i, 0, 0]), f.count)
            f.reset()
        print()


def second_assignment():
    functions = [func.get('f1'), func.get('f2'), func.get('f3'), func.get(
        'f4')]
    algorithms = [opt.coordinate_descent, opt.hooke_jeeves_search,
                  opt.nelder_mead]

    x_0 = [func.start for func in functions]
    x_0[2] = 5 * [0]

    gg = []
    for f, x_0 in zip(functions, x_0):

        for alg in algorithms:
            x = alg(f, x_0)

            row = [f.name, x_0, x, alg.__name__, f.count]
            f.reset()
            gg.append(row)

    df = pd.DataFrame(gg, columns=["f", "x_0", "x", "algo", "iter"])

    with pd.option_context('display.max_rows', None, 'display.max_columns',
                           None):
        print(df)


def third_task():
    f = func.get('f4')

    x1, x2 = 5, 5

    result1 = opt.hooke_jeeves_search(f, [x1, x2])
    result2 = opt.nelder_mead(f, [x1, x2])

    print('Min Hooke Jeeves: ', result1)
    print('Min Nelder Mead: ', result2)


def fourth_task():
    f = func.get('f1')

    x = [0.5, 0.5]

    for i in range(1, 21):
        print("step %d: min = %s, count = %d" % (i, str(opt.nelder_mead(f, x,
                                                                       shift=i)),
                                                 f.count))

    x = [20, 20]

    print('\nWith point (20, 20)')
    for i in range(1, 21):
        print("step %d: min = %s, count = %d" % (i, str(opt.nelder_mead(f, x,
                                                                       shift=i)),
                                                 f.count))


def fifth_task():
    f = func.get('f6')
    hits = 0
    max_iter = 1000

    for i in range(max_iter):
        x = [random.uniform(-50, 50), random.uniform(-50, 50)]

        tmp = abs(f(opt.nelder_mead(f, x)))

        if tmp < 1:
            hits += 1e-4

    print("Vjerojatnost nalaženja globalnog optimuma je %.2f%%" % (
            100 * hits / max_iter))


if __name__ == '__main__':
    num = int(input('Unesite broj zadatka:'))

    if num == 1:
        first_task()
    elif num == 2:
        second_assignment()
    elif num == 3:
        third_task()
    elif num == 4:
        fourth_task()
    elif num == 5:
        fifth_task()
    else:
        print('Pogrešan unos!')
