"""
"""
import math


class Function:
    """
    """

    def __init__(self, name, f, x_0, x_min, f_min):
        """

        :param name:
        :param f:
        :param x_0:
        :param x_min:
        :param f_min:
        """
        self.name = name
        self._f = f
        self._x_0 = x_0
        self._x_min = x_min
        self._f_min = f_min
        self._count = 0

    def eval(self, *args):
        """

        :param params:
        :return:
        """
        self._count += 1
        return self._f(*args)

    def reset(self):
        """

        :return:
        """
        self._count = 0

    @property
    def start(self):
        """

        :return:
        """
        return self._x_0

    @property
    def minimum(self):
        """

        :return:
        """
        return self._f_min

    @property
    def x_min(self):
        """

        :return:
        """
        return self._x_min

    @property
    def function(self):
        """

        :return:
        """
        return self._f

    @property
    def count(self):
        """

        :return:
        """
        return self._count

    def __call__(self, args):
        """

        :param args:
        :param kwargs:
        :return:
        """
        return self.eval(*args)


def _schaffer(*x):
    """

    :param x:
    :return:
    """
    up = math.sin(math.sqrt(sum([i ** 2 for i in x]))) ** 2 - 0.5
    down = (1 + 0.001 * sum([i ** 2 for i in x])) ** 2

    return 0.5 + up / down


FUNCTIONS = {
    'f1': Function('f1',
                   lambda x, y: 100 * ((y - x * x) ** 2) + ((1 - x) ** 2),
                   [-1.9, 2], [1, 1], 0),
    'f2': Function('f2',
                   lambda x, y: (x - 4) ** 2 + 4 * (y - 2) ** 2,
                   [0.1, 0.3], [4, 2], 0),
    'f3': Function('f3',
                   lambda *x: sum([(xi - i) ** 2 for i, xi in enumerate(x, 1
                                                                        )]),
                   [0], None, 0),
    'f4': Function('f4',
                   lambda x, y: abs((x - y) * (x + y)) + math.sqrt(x * x +
                                                                   y * y),
                   [5.1, 1.1], [0, 0], 0),
    'f6': Function('f6',
                   _schaffer,
                   None, [0], 0)
}


def get(name):
    """

    :param name:
    :return:
    """
    return FUNCTIONS[name]
