"""

"""
import abc
import genetic.selections as selections
import genetic.crossovers as crossovers
import genetic.utils as utils


class GeneticAlgorithm(abc.ABC):
    """

    """

    def __init__(self, data, max_iter=100000, min_error=1e-6, k=3):
        """

        :param pop_size:
        :param max_iter:
        :param min_error:
        """
        self.max_iter = max_iter
        self.min_error = min_error
        self.data = data

        self.pop_size = len(data)
        self.population = sorted(self.data)
        self._errors = []
        self.k = k

    def run(self, trace=False):
        """

        :param data:
        :return:
        """
        error = 0

        for i in range(self.max_iter):
            error = self.population[0].error()

            if i % 1000 == 0:
                if trace:
                    print("Iteration " + str(i) + ", error=" + str(error))

                self._errors.append([i, error])

            if abs(error) < self.min_error:
                return error, self.population[0].data

            self.population = sorted(self.get_next_generation())

        return error, self.population[0].eval()

    @abc.abstractmethod
    def get_next_generation(self):
        """

        :return:
        """
        raise NotImplementedError()

    @property
    def errors(self):
        """

        :return:
        """
        return self._errors


class EliminationGeneticAlgorithm(GeneticAlgorithm):
    """

    """

    def get_next_generation(self):
        """

        :return:
        """
        tour = sorted(selections.tour_selection(self.population, self.k),
                      reverse=True)
        self.population.remove(tour[0])
        new_chromosome = crossovers.uniform_crossover(tour[-1], tour[-2])
        new_chromosome.mutate()
        self.population.append(new_chromosome)

        return self.population
