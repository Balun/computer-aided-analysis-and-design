import genetic.genetic_algorithm as genetic
import genetic.chromosome as chromosome
import genetic.utils as utils


def first():
    pop1 = chromosome.generate_new_binary_population(1000, 5, utils.f3)
    gen1 = genetic.EliminationGeneticAlgorithm(pop1)
    print(gen1.run())


if __name__ == '__main__':
    first()
