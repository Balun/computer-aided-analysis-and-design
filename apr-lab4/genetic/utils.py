"""

"""
import random
import numpy as np


def read_data(file_path, sep='\t'):
    """

    :param file_path:
    :param sep:
    :return:
    """
    data = []

    with open(file_path) as fp:
        for line in fp:
            data.append([float(tmp) for tmp in line.split(sep)])

    return data


def random_params(lower=-4, upper=4, n=4):
    """

    :param lower:
    :param upper:
    :param n:
    :return:
    """
    tuple([random.uniform(lower, upper) for _ in range(n)])


def f1(x):
    return 100 * (x[1] - x[0] ** 2) ** 2 + (1 - x[0]) ** 2


def f3(x):
    return np.sum(np.array([(tmp - i) ** 2 for i, tmp in enumerate(x,
                                                                   start=1)]))


def f6(x):
    up = np.sin(np.sqrt(np.sum(np.array([tmp ** 2 for tmp in x])))) ** 2 - 0.5
    down = (1 + 0.001 * np.sum(np.array([tmp ** 2 for tmp in x]))) ** 2
    return 0.5 + up / down


def f7(x):
    first = np.sum(np.array([tmp ** 2 for tmp in x])) ** 0.25
    second = (1 + np.sin(
        50 * np.sum(np.array([tmp ** 2 for tmp in x])) ** 0.1) ** 2)
    return first * second
