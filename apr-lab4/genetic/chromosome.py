"""
"""

import abc
import numpy as np
import random

from functools import reduce


def generate_new_binary_population(n, dim, error_function, lower=-50,
                                   upper=150,
                                   decimals=4, p=0.1):
    """

    :param n:
    :param dim:
    :param error_function:
    :param lower:
    :param upper:
    :param decimals:
    :return:
    """

    return [generate_new_binary_chromosome(dim, error_function, lower, upper,
                                           decimals, p) for _ in range(n)]


def generate_new_float_population(n, dim, error_function, lower=-50,
                                  upper=150, p=0.1):
    """

    :param n:
    :param dim:
    :param error_function:
    :param lower:
    :param upper:
    :return:
    """
    return [generate_new_float_chromosome(dim, error_function, lower,
                                          upper, p) for _ in range(n)]


def generate_new_binary_chromosome(dim, error_function, lower=-50, upper=150,
                                   decimals=3, p=0.1):
    """

    :param dim:
    :param error_function:
    :param lower:
    :param upper:
    :param decimals:
    :return:
    """
    n = np.int(
        np.ceil(np.log2(np.floor(1 + (upper - lower) * 10 ** decimals))))

    return BinaryChromosome(np.random.randint(2, size=[dim, n]),
                            error_function, lower, upper)


def generate_new_float_chromosome(dim, error_function, lower=-50, upper=150,
                                  p=0.1):
    """

    :param dim:
    :param error_function:
    :param lower:
    :param upper:
    :return:
    """
    new_data = np.random.uniform(lower, upper, dim)
    return FloatChromosome(new_data, error_function)


class Chromosome(abc.ABC):
    """

    """

    def __init__(self, data):
        """

        :param data:
        """
        self._data = data
        self._error = None

    @property
    @abc.abstractmethod
    def error(self):
        """

        :return:
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def mutate(self):
        """
        """
        raise NotImplementedError()

    @abc.abstractmethod
    def get_new_instance(self, data):
        """

        :param data:
        :return:
        """
        raise NotImplementedError()

    def eval(self):
        """

        :return:
        """
        return self.data

    @property
    def data(self):
        """

        :return:
        """
        return self._data

    def __eq__(self, other):
        """

        :param other:
        :return:
        """
        return self.error() == other.error()

    def __gt__(self, other):
        """

        :param other:
        :return:
        """
        return self.error() > other.error()

    def __lt__(self, other):
        """

        :param other:
        :return:
        """
        return self.error() < other.error()

    def __ge__(self, other):
        """

        :param other:
        :return:
        """
        return self.error() >= other.error()

    def __le__(self, other):
        """

        :param other:
        :return:
        """
        return self.error() <= other.error()

    def __getitem__(self, item):
        """

        :param item:
        :return:
        """
        return self._data[item]

    def __len__(self):
        """

        :return:
        """
        return len(self.data)

    def __iter__(self):
        """

        :return:
        """
        return iter(self.data)


class BinaryChromosome(Chromosome):
    """

    """

    def __init__(self, data, error_function, lower, upper, p=0.1):
        """

        """
        super().__init__(np.array(data))
        self._error_function = error_function
        self.p = p
        self._error = None
        self.u = upper
        self.l = lower
        self._value = None

    def error(self):
        """

        :return:
        """
        if self._error is None:
            self._error = self._error_function(self.eval())

        return self._error

    def mutate(self):
        """
        """
        self._data = np.array([np.logical_not(x) if (self.p >
                                                     np.random.uniform(0, 1))
                               else x for x in self.data])

    def eval(self):
        """

        :return:
        """
        if self._value is None:
            self._value = (self.u - self.l) / (
                    2 ** len(self.data[0]) - 1) * reduce(
                lambda x, y: 2 * x + y, self.data.T) + self.l

        return self._value

    def get_new_instance(self, data):
        """

        :param data:
        :return:
        """
        return BinaryChromosome(data, self._error_function, self.l,
                                self.u, self.p)


class FloatChromosome(Chromosome):
    """

    """

    def __init__(self, data, error_function, p=0.05):
        super().__init__(np.array(data))

        self._error_function = error_function
        self.p = p

    def error(self):
        """

        :return:
        """
        if self._error is None:
            self._error = self._error_function(self.data)

        return self._error

    def mutate(self):
        """

        """
        self._data = np.array([x + np.random.normal() if (self.p >
                                                          random.uniform(0,
                                                                         1)) else x
                               for x in self.data])

    def get_new_instance(self, data):
        """

        :param data:
        :return:
        """
        return FloatChromosome(data, self._error_function, self.p)
