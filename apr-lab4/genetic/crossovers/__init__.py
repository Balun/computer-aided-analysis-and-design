"""
"""
import random


def t_point_crossover(first, second, t=1):
    """

    :param first:
    :param second:
    :param t:
    :return:
    """
    if (t < 1) or (t > (len(first) - 1)):
        raise ValueError("Value of parameter k must be greater or equal to 1.")

    elif len(first) != len(second):
        raise ValueError("Chromosomes must be of equal length")

    elif type(first) != type(second):
        raise ValueError("chromosomes must be of same type.")

    new_data = []

    if t == 1:
        point = random.uniform(0, len(first))
        new_data += first.data[:point] + second.data[point:]

        return first.get_new_instance(new_data)

    points = sorted(random.sample(range(1, len(first)), t))

    i = 0
    for k in points:
        if i == 0:
            new_data += first.data[:k]

        elif i % 2 == 0:
            new_data += first.data[points[i - 1]: k]

        elif i % 2 == 1:
            new_data += second.data[points[i - 1]: k]

        i += 1

    if len(first.data) != len(new_data):
        if i % 2 == 0:
            new_data += first.data[points[i - 1]:]

        else:
            new_data += second.data[points[i - 1]:]

    return first.get_new_instance(new_data)


def uniform_crossover(first, second):
    if len(first) != len(second):
        raise ValueError("Chromosomes must be of same length!")

    if type(first) != type(second):
        raise ValueError("chromosomes must be of same type.")

    new_data = []

    for i, j in zip(first, second):
        p = random.uniform(0, 1)
        new_data.append(i if (p < 0.5) else j)

    return first.get_new_instance(new_data)
